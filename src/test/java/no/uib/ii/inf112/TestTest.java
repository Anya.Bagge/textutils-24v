package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra1 = (int)Math.ceil((width - text.length()) / 2.0);
			int extra2 = (int)Math.floor((width - text.length()) / 2.0);
			return " ".repeat(extra1) + text + " ".repeat(extra2);
		}

		public String flushRight(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
	}

	@Test
	void testCenter1() {
		assertEquals(" foo ", aligner.center("foo", 5));
	}

	@Test
	void testCenter2() {
		assertEquals("  A  ", aligner.center("A", 5));
	}
	@Test
	void testCenterEven() {
		String result = aligner.center("AA", 5);
		assertTrue(result.equals(" AA  ") || result.equals("  AA "));
	}
	/*
	@Test
	void testCenterEven1() {
		assertEquals(" AA  ", aligner.center("AA", 5));
	}
	@Test
	void testCenterEven2() {
		assertEquals("  AA ", aligner.center("AA", 5));
	}
	@Test
	void testCenterEven3() {
		assertEquals(" AA ", aligner.center("AA", 5));
	}
	*/
}
